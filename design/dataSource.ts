const componentData = {
  titleCaption: "Poukaz na sezení s terapeutem",
  title: "Prostor jen pro Vás",
  code: "PROMO-CODE",
  linkCaption: "Uplatnit poukaz",
  linkUrl: "https://terap.io",
};

export default componentData;
