import React from "react";
import { Button, Typography } from "@material-ui/core";

export interface TestButtonProps {
  caption: string;
  onSubmit: () => void;
}

const TestButton: React.FC<TestButtonProps> = ({
  caption,
  onSubmit,
}: TestButtonProps) => (
  <Button color="primary" variant="outlined" onClick={onSubmit}>
    <Typography variant="body2">{caption}</Typography>
  </Button>
);

export default TestButton;
