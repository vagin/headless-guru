import createPalette from "@material-ui/core/styles/createPalette";

export default createPalette({
  primary: {
    light: "#FFFAEF",
    main: "#FFB400",
    dark: "#D29505",
  },
  secondary: {
    main: "#F10A34",
  },
  error: {
    main: "#F10A34",
  },
  text: {
    primary: "#000000",
    secondary: "#626261",
    hint: "#8C8C8C",
  },
  action: {
    disabled: "#91959E",
    disabledBackground: "#ECEDEF",
    hoverOpacity: 0.1,
  },
  background: {
    default: "#ffffff",
  },
  divider: "#CFD0E6",
  customBg: {
    success: "#E6FBDD",
    error: "#FFEDED",
    gossamer: "#49A48B",
    catskillWhite: "#E4E7EB",
    linkWater: "#E0E8F9",
    tequila: "#FFE7CC",
    zircon: "#F2F4FF",
    dawn: "#FFF7E6",
    alabaster: "#F7F7F7",
    hintGreen: "#E6FFF2",
    cosmos: "#FFCFCC",
    titan: "#FCFCFF",
    pippin: "#ffe6e6",
    aliceBlue: "#F7F9FB",
  },
  customCol: {
    success: "#41C20A",
    error: "#F55C5C",
    cornflowerBlue: "#D9D9FF",
    chablis: "#FFF2F2",
    sunglow: "#FFC933",
    shamrock: "#2EE696",
    mountain: "#20A964",
    jungle: "#24B36B",
    silver: "#BFBFBF",
    scorpion: "#5A5A5A",
    alto: "#DEDEDE",
    dustyGray: "#979797",
    mercury: "#E2E2E2",
    moody: "#707CCC",
    mexican: "#b32424",
    solitude: "#ECEDEF",
    manatee: "#91959E",
  },
});
